@extends('layouts.app')
@section('content')
<div>
  <h1>Update Student Record</h1>
  <form action="/update" method="POST">
    @csrf
      <div>
        <input type="hidden" value="{{ $data -> id }}" name="id">
      </div>
      <div>
        <label >First Name</label>
        <input type="text" value="{{ $data -> first_name }}" name="fname">
      </div>
      <br>
      <div>
        <label>Last Name</label>
        <input type="text" value="{{ $data -> last_name }}" name="lname">
      </div> <br>
      <div>
        <label>Location</label>
        <input type="text" value="{{ $data -> location }}" name="loc"/>
      </div> <br>
      <div>
        <button type="submit" name="update_stud">Update Record</button>
      </div>
    </form>

</div>
@endsection
