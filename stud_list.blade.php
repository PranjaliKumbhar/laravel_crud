@extends('layouts.app')
@section('content')
        <!--<div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
            @if (Route::has('login'))
                <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                    @auth
                        <a href="{{ url('/home') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Home</a>
                    @else
                        <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                        @endif
                    @endauth
                </div>
            @endif -->

            <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
                <div class="flex justify-center pt-8 sm:justify-start sm:pt-0">
                   List Student 
                </div>
                <div>
                    <form action="/add">
                        <button type="submit" name="add">Add Record</button>
                    </form>
                </div>

                <div class="mt-8 dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
                    <table>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Location </th>
                        </tr>
                        @foreach ($data as $s)
                            <tr>
                                <td>{{ $s -> first_name }}</td>
                                <td>{{ $s -> last_name }}</td>
                                <td>{{ $s -> location }}</td>
                                <td>
                                    <form action="/delete/{{ $s -> id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button>Delete</button>
                                    </form>
                                </td>
                                <td>
                                    <form action="/update/{{ $s -> id }}">
                                        @csrf
                                        
                                        <button>Update</button>
                                    </form>

                                </td>
                                
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
@endsection