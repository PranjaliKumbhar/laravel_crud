@extends('layouts.layout')
@section('content')
<div>
  <h1>Add a student</h1>
  <form action="/add" method="POST">
    @csrf
      <div>
        <label >First Name</label>
        <input type="text" name="fname">
      </div>
      <br>
      <div>
        <label>Last Name</label>
        <input type="text" name="lname">
      </div> <br>
      <div>
        <label>Location</label>
        <input type="text" name="loc">
      </div> <br>
      <div>
        <button type="submit" name="add_stud">Insert</button>
        <a href="/list">Back</a>
      </div>
    </form>

</div>
@endsection